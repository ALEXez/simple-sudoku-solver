#ifndef LINE_H
#define LINE_H

#include <vector>
#include "cell.h"

class Line {
public:
    Line(std::vector<Cell *> *cells);
    Line(const Line &) = delete;
    Line &operator=(const Line &) = delete;
    ~Line();
    unsigned char SetImpossibleToLine(unsigned char number);
    unsigned char Pass();
    bool CheckForConflict() const;
private:
    std::vector<Cell *> *_cells;
};

#endif // LINE_H
