#ifndef GLOBAL_H
#define GLOBAL_H

const unsigned char MAX_NUMBER = 9; // 9 or 16
const unsigned char IMPOSSIBLE_NUMBER = MAX_NUMBER * MAX_NUMBER + 1;

#endif // GLOBAL_H
