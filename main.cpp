#include <iostream>
#include <fstream>
#include "cell.h"
#include "line.h"
#include "field.h"
#include "global.h"

std::fstream outputFile;

enum class ErrorType : unsigned char {
    INPUT_FILE_OPEN_ERROR, INPUT_FILE_INCORRECT_ERROR, UNSOLVABLE_ERROR
};

void PrintError(ErrorType error);
void PrintDoneField(Field *doneField);

int main(int argc, char *argv[]) {
    outputFile.open("sudoku.out.txt", std::ios::out);
    std::fstream inputFile;
    if (argc == 2) {
        inputFile.open(argv[1], std::ios::in);
    } else {
        std::cout << "Sudoku-solver v1.1 by ALEXez\nUsage:\n\tLinux: sudoku_lin sudokufile\n\tWindows: sudoku_win.exe sudokufile\nSource on http://code.alexez.com/sudoku-solver" << std::endl;
        return 1;
    }
    std::vector<unsigned char> numbers;
    if (!inputFile.is_open()) {
        PrintError(ErrorType::INPUT_FILE_OPEN_ERROR);
        return 1;
    } else {
        unsigned int tempNumber;
        while (inputFile >> tempNumber) {
            numbers.push_back(tempNumber);
            inputFile.get();
        }
    }
    if (numbers.size() != MAX_NUMBER * MAX_NUMBER) {
        PrintError(ErrorType::INPUT_FILE_INCORRECT_ERROR);
        return 1;
    }
    bool isEnd = false;
    std::vector<Field *> fields;
    fields.push_back(new Field(numbers));
    while (!isEnd) {
        if (fields.size() == 0) {
            PrintError(ErrorType::UNSOLVABLE_ERROR);
            return 1;
        }
        for (unsigned int i = 0; i < fields.size(); i++) {
            FieldStatus fieldStatus = fields[i]->Check();
            if (fieldStatus == FieldStatus::NOT_DONE)
                fields[i]->Step();
            else {
                if (fieldStatus == FieldStatus::UNSOLVABLE) {
                    delete fields[i];
                    fields.erase(fields.begin() + i);
                } else if (fieldStatus == FieldStatus::DONE) {
                    if (fields[i]->DoneCheck()) {
                        PrintDoneField(fields[i]);
                        isEnd = true;
                    } else {
                        delete fields[i];
                        fields.erase(fields.begin() + i);
                    }
                } else {
                    Field *newField = fields[i]->CreateNonAmbivalentField();
                    if (newField != nullptr)
                        fields.push_back(newField);
                }
            }
        }
    }
    for (Field *item : fields) {
        delete item;
    }
    return 0;
}

void PrintError(ErrorType error) {
    if (outputFile.is_open()) {
        outputFile << "ERROR:" << std::endl;
        switch (error) {
        case ErrorType::INPUT_FILE_OPEN_ERROR:
            outputFile << "Input file can not be opened";
            break;
        case ErrorType::INPUT_FILE_INCORRECT_ERROR:
            outputFile << "Input file is incorrect";
            break;
        case ErrorType::UNSOLVABLE_ERROR:
            outputFile << "This sudoku is unsolvable";
            break;
        }
        outputFile.close();
    } else {
        std::cout << "ERROR:" << std::endl;
        switch (error) {
        case ErrorType::INPUT_FILE_OPEN_ERROR:
            std::cout << "Input file can not be opened";
            break;
        case ErrorType::INPUT_FILE_INCORRECT_ERROR:
            std::cout << "Input file is incorrect";
            break;
        case ErrorType::UNSOLVABLE_ERROR:
            std::cout << "This sudoku is unsolvable";
            break;
        }
    }
}

void PrintDoneField(Field *doneField) {
    if (outputFile.is_open()) {
        outputFile << "ANSWER:" << std::endl;
        for (unsigned char i = 0; i < MAX_NUMBER; i++) {
            for (unsigned char j = 0; j < MAX_NUMBER; j++) {
                outputFile << '\t' << (unsigned int)doneField->GetCell(i * MAX_NUMBER + j)->GetNumber();
            }
            outputFile << std::endl;
        }
        outputFile.close();
    } else {
        std::cout << "ANSWER:" << std::endl;
        for (unsigned char i = 0; i < MAX_NUMBER; i++) {
            for (unsigned char j = 0; j < MAX_NUMBER; j++) {
                std::cout << '\t' << (unsigned int)doneField->GetCell(i * MAX_NUMBER + j)->GetNumber();
            }
            std::cout << std::endl;
        }
    }
}
