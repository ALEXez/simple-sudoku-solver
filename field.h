#ifndef FIELD_H
#define FIELD_H

#include <vector>
#include "cell.h"
#include "line.h"
#include "global.h"

enum class FieldStatus : unsigned char {
    DONE, NOT_DONE, UNSOLVABLE, AMBIVALENT
};

class Field {
public:
    Field(std::vector<unsigned char> &numbers);
    ~Field();
    Field(const Field &other);
    Field &operator=(const Field &) = delete;
    inline Cell *GetCell(unsigned char id) const {
        return (*_cells)[id];
    };
    FieldStatus Check() const;
    void Step();
    bool DoneCheck() const;
    Field *CreateNonAmbivalentField();
private:
    void SetCell(unsigned char newCellId, Cell *newCell);
    unsigned char GetAmbivalentCell() const;
    Line *GenerateLineByRow(unsigned char rowNumber) const;
    Line *GenerateLineByColumn(unsigned char columnNumber) const;
    Line *GenerateLineBySquare(unsigned char squareNumber) const;
    inline unsigned char GetRowNumberByCellId(unsigned char id) const {
        return (unsigned char)(id / MAX_NUMBER);
    };
    inline unsigned char GetColumnNumberByCellId(unsigned char id) const {
        return id - GetRowNumberByCellId(id) * MAX_NUMBER;
    };
    inline unsigned char GetSquareNumberByCellId(unsigned char id) const {
        return (char)(GetRowNumberByCellId(id) / 3) * 3 + (char)(GetColumnNumberByCellId(id) / 3);
    };
    std::vector<Cell *> *_cells;
    FieldStatus _fieldStatus;
};

#endif // FIELD_H
