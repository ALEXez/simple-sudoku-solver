#include "field.h"

Field::Field(std::vector<unsigned char> &numbers) : _cells(new std::vector<Cell *>), _fieldStatus(FieldStatus::NOT_DONE) {
    for (unsigned char item : numbers)
        (*_cells).push_back(new Cell(item));
}

Field::~Field() {
    for (Cell *item : *_cells)
        delete item;
    delete _cells;
}

Field::Field(const Field &other) : _cells(new std::vector<Cell *>), _fieldStatus(FieldStatus::NOT_DONE) {
    for (unsigned char i = 0; i < MAX_NUMBER * MAX_NUMBER; i++) {
        _cells->push_back(new Cell(*other.GetCell(i)));
    }
}

void Field::SetCell(unsigned char newCellId, Cell *newCell) {
    delete(*_cells)[newCellId];
    (*_cells)[newCellId] = newCell;
}

FieldStatus Field::Check() const {
    if (_fieldStatus == FieldStatus::AMBIVALENT)
        return FieldStatus::AMBIVALENT;
    else if (_fieldStatus == FieldStatus::UNSOLVABLE)
        return FieldStatus::UNSOLVABLE;
    unsigned char count = 0;
    for (unsigned char i = 0; i < MAX_NUMBER * MAX_NUMBER; i++) {
        if ((*_cells)[i]->Check() == CellStatus::INVALID)
            return FieldStatus::UNSOLVABLE;
        if ((*_cells)[i]->GetNumber() != 0)
            count++;
    }
    if (count == MAX_NUMBER * MAX_NUMBER) {
        return FieldStatus::DONE;
    } else if (count == 0) {
        return FieldStatus::UNSOLVABLE;
    } else {
        return FieldStatus::NOT_DONE;
    }
}

void Field::Step() {
    unsigned char result = 0;
    for (unsigned char i = 0; i < MAX_NUMBER; i++) {
        Line *tempLine = GenerateLineByRow(i);
        result += tempLine->Pass();
        delete tempLine;
        tempLine = GenerateLineByColumn(i);
        result += tempLine->Pass();
        delete tempLine;
        tempLine = GenerateLineBySquare(i);
        result += tempLine->Pass();
        delete tempLine;
    }
    if (result == 0) {
        _fieldStatus = FieldStatus::AMBIVALENT;
    }
}

bool Field::DoneCheck() const {
    for (unsigned char i = 0; i < MAX_NUMBER; i++) {
        bool result = true;
        Line *tempLine = GenerateLineByRow(i);
        result = result & tempLine->CheckForConflict();
        delete tempLine;
        tempLine = GenerateLineByColumn(i);
        result = result & tempLine->CheckForConflict();
        delete tempLine;
        tempLine = GenerateLineBySquare(i);
        result = result & tempLine->CheckForConflict();
        delete tempLine;
        if (!result)
            return false;
    }
    return true;
}

unsigned char Field::GetAmbivalentCell() const {
    for (unsigned char i = 2; i < MAX_NUMBER + 1; i++)
        for (unsigned char j = 0; j < MAX_NUMBER * MAX_NUMBER; j++)
            if ((*_cells)[j]->GetNumber() == 0)
                if ((*_cells)[j]->GetOptionsCount() == i)
                    return j;
    return IMPOSSIBLE_NUMBER;
}

Field *Field::CreateNonAmbivalentField() {
    unsigned char keyCellNumber = GetAmbivalentCell();
    if (keyCellNumber == IMPOSSIBLE_NUMBER) {
        _fieldStatus = FieldStatus::UNSOLVABLE;
        return nullptr;
    }
    Cell *keyCell = (*_cells)[keyCellNumber];
    std::vector<bool> *keyCellOptions = keyCell->GetOptionsVector();
    Cell *newCell = new Cell(*keyCell);
    std::vector<bool> *newCellOptions = newCell->GetOptionsVector();
    bool firstOption = true;
    for (unsigned char i = 0; i < MAX_NUMBER; i++) {
        if ((*keyCellOptions)[i]) {
            if (firstOption) {
                (*keyCellOptions)[i] = false;
                firstOption = false;
            } else {
                (*newCellOptions)[i] = false;
            }
        }
    }
    Field *newField = new Field(*this);
    newField->SetCell(keyCellNumber, newCell);
    keyCell->AutoPreparing();
    newCell->AutoPreparing();
    _fieldStatus = FieldStatus::NOT_DONE;
    return newField;
}

Line *Field::GenerateLineByRow(unsigned char rowNumber) const {
    std::vector<Cell *> *tempCellVector = new std::vector<Cell *>;
    for (unsigned char i = 0; i < MAX_NUMBER * MAX_NUMBER; i++) {
        if (GetRowNumberByCellId(i) == rowNumber)
            tempCellVector->push_back((*_cells)[i]);
        if (tempCellVector->size() == MAX_NUMBER)
            break;
    }
    Line *temp = new Line(tempCellVector);
    return temp;
}

Line *Field::GenerateLineByColumn(unsigned char columnNumber) const {
    std::vector<Cell *> *tempCellVector = new std::vector<Cell *>;
    for (unsigned char i = 0; i < MAX_NUMBER * MAX_NUMBER; i++)
        if (GetColumnNumberByCellId(i) == columnNumber)
            tempCellVector->push_back((*_cells)[i]);
    Line *temp = new Line(tempCellVector);
    return temp;
}

Line *Field::GenerateLineBySquare(unsigned char squareNumber) const {
    std::vector<Cell *> *tempCellVector = new std::vector<Cell *>;
    for (unsigned char i = 0; i < MAX_NUMBER * MAX_NUMBER; i++) {
        if (GetSquareNumberByCellId(i) == squareNumber)
            tempCellVector->push_back((*_cells)[i]);
        if (tempCellVector->size() == MAX_NUMBER)
            break;
    }
    Line *temp = new Line(tempCellVector);
    return temp;
}
