#ifndef CELL_H
#define CELL_H

#include <vector>
#include "global.h"

enum class CellStatus : unsigned char {
    DONE, NOT_DONE, ONE_OPTION, INVALID
};

class Cell {
public:
    Cell();
    Cell(unsigned char number);
    ~Cell();
    void SetImpossible(unsigned char number);
    void AutoPreparing();
    CellStatus Check() const;
    inline unsigned char GetOptionsCount() const {
        unsigned char result = 0;
        for (bool item : _options)
            if (item)
                result++;
        return result;
    }
    inline unsigned char GetNumber() const {
        return _number;
    };
    inline std::vector<bool> *GetOptionsVector() {
        return &_options;
    }
private:
    unsigned char _number;
    std::vector<bool> _options;
};

#endif // CELL_H
