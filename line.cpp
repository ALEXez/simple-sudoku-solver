#include "line.h"

Line::Line(std::vector<Cell *> *cells) : _cells(cells) {}

Line::~Line() {
    delete _cells;
}

unsigned char Line::SetImpossibleToLine(unsigned char number) {
    unsigned char result = 0;
    for (Cell *item : *_cells) {
        unsigned char start = item->GetNumber();
        item->SetImpossible(number);
        unsigned char end = item->GetNumber();
        if (start != end)
            result++;
    }
    return result;
}

unsigned char Line::Pass() {
    unsigned char result = 0;
    for (Cell *item : *_cells) {
        if (item->GetNumber() != 0)
            result += SetImpossibleToLine(item->GetNumber());
    }
    return result;
}

bool Line::CheckForConflict() const {
    for (Cell *firstItem : *_cells)
        for (Cell *secondItem : *_cells)
            if (firstItem != secondItem)
                if (firstItem->GetNumber() == secondItem->GetNumber())
                    return false;
    return true;
}
