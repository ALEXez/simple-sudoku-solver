#include "cell.h"

Cell::Cell() : _number(0), _options(MAX_NUMBER, true) {}

Cell::Cell(unsigned char number) : _number(number), _options(MAX_NUMBER, true) {}

Cell::~Cell() {}

void Cell::SetImpossible(unsigned char number) {
    if (_number == 0) {
        _options[number - 1] = false;
        AutoPreparing();
    }
}

void Cell::AutoPreparing() {
    unsigned char temp = 0;
    for (unsigned char i = 0; i < MAX_NUMBER; i++) {
        if (_options[i] == true) {
            if (temp == 0)
                temp = i + 1;
            else {
                temp = 0;
                break;
            }
        }
    }
    if (temp != 0) {
        _number = temp;
    }
}

CellStatus Cell::Check() const {
    if (_number != 0)
        return CellStatus::DONE;
    for (unsigned char i = 0; i < MAX_NUMBER; i++) {
        if (_options[i] == true) {
            return CellStatus::NOT_DONE;
        }
    }
    return CellStatus::INVALID;
}
